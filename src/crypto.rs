use std::vec;

use anyhow::Result;
use chacha20poly1305::{
    aead::{
        generic_array::{
            typenum::{UInt, UTerm},
            GenericArray,
        },
        Aead, OsRng,
    },
    consts::{B0, B1},
    AeadCore, ChaCha20Poly1305, Key, KeyInit,
};

type Nonce = GenericArray<u8, UInt<UInt<UInt<UInt<UTerm, B1>, B1>, B0>, B0>>;

pub fn encrypt(data: &[u8], key: &String) -> Result<Vec<u8>> {
    let key = Key::from_slice(key.as_bytes());
    let cipher = ChaCha20Poly1305::new(key);
    let nonce: Nonce = ChaCha20Poly1305::generate_nonce(&mut OsRng);
    let cipher_data = cipher.encrypt(&nonce, data).unwrap();

    let mut buf = vec![];
    buf.extend_from_slice(&nonce);
    buf.extend_from_slice(&cipher_data);

    Ok(buf)
}

pub fn decrypt(data: &[u8], key: &String) -> Result<Vec<u8>> {
    let key = Key::from_slice(key.as_bytes());
    let cipher = ChaCha20Poly1305::new(&key);
    let nonce = GenericArray::clone_from_slice(&data[0..12]);
    let cipher_data = &data[12..];
    let plain_data = cipher.decrypt(&nonce, cipher_data).unwrap();

    Ok(plain_data.to_vec())
}
