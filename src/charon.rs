use std::{
    fs,
    io::{self, Write},
    path::{Path, PathBuf},
    time::Instant,
};

use anyhow::Result;
use s3::{serde_types::Object, Bucket};

use crate::{config::Config, crypto, s3::*};

pub async fn run(config: &Config) -> Result<()> {
    let scan_path = &config.backup_dir;
    let bucket = get_store(config).await?;

    println!("Scanning {}", scan_path);
    let local_backups = list_local_backups(scan_path).await?;
    println!("Listing remote backups");
    let remote_backups = list_remote_backups(&bucket).await?;
    println!("Comparing local and remote backups");
    let (to_upload, to_encrypt) = diff_backups(local_backups, remote_backups).await?;
    println!(
        "{} files to upload as plaintext and {} files to encrypt",
        to_upload.len(),
        to_encrypt.len()
    );

    let global_start = Instant::now();
    // Upload plaintext files to S3
    for file in to_upload {
        let file_name = if let Some(file_name) = file.file_name() {
            file_name.to_string_lossy()
        } else {
            eprint!("File name is empty: {}", file.display());
            continue;
        };

        print!("Uploading {}...", file_name);
        io::stdout().flush()?;
        let step_start = Instant::now();
        let file_content = fs::read(&file)?;
        bucket.put_object(file_name, &file_content).await?;
        println!(" : {}s", step_start.elapsed().as_secs());
    }

    for file in to_encrypt {
        let encrypted_filename = file.with_extension("zst.charon");
        let file_name = if let Some(file_name) = encrypted_filename.file_name() {
            file_name.to_string_lossy()
        } else {
            eprint!("File name is empty: {}", file.display());
            continue;
        };

        let file_content = fs::read(&file)?;

        print!("Encrypting {}...", file_name);
        io::stdout().flush()?;
        let step_start = Instant::now();
        let encrypted_content = crypto::encrypt(&file_content, &config.encryption_key)?;
        println!(" : {}s", step_start.elapsed().as_secs());

        print!("Uploading {}", file_name);
        io::stdout().flush()?;
        let step_start = Instant::now();
        bucket.put_object(file_name, &encrypted_content).await?;
        println!(" : {}s", step_start.elapsed().as_secs());
    }
    let end = Instant::now();
    println!(
        "Total duration {}mins {}s",
        end.duration_since(global_start).as_secs() / 60,
        end.duration_since(global_start).as_secs() % 60
    );

    Ok(())
}

async fn list_local_backups<P>(path: P) -> Result<Vec<PathBuf>>
where
    P: AsRef<Path>,
{
    let backups_files: Vec<PathBuf> = fs::read_dir(path)?
        .filter_map(|entry| {
            let entry = entry.unwrap();
            let path = entry.path();
            if !path.is_file() {
                return None;
            }

            Some(path)
        })
        .collect();

    Ok(backups_files)
}

async fn list_remote_backups(bucket: &Bucket) -> Result<Vec<Object>> {
    let namespace = &bucket.list("".to_string(), None).await?[0];

    Ok(namespace.contents.clone())
}

async fn diff_backups(
    local: Vec<PathBuf>,
    remote: Vec<Object>,
) -> Result<(Vec<PathBuf>, Vec<PathBuf>)> {
    let mut to_upload = Vec::new();
    let mut to_encrypt = Vec::new();

    for local_file in local {
        match local_file.extension() {
            Some(ext) if ext == "zst" => {
                let charon_file = local_file.with_extension("zst.charon");
                let remote_file = remote
                    .iter()
                    .find(|f| f.key == charon_file.file_name().unwrap().to_string_lossy());

                if remote_file.is_none() {
                    to_encrypt.push(local_file);
                }
            }
            _ => {
                let remote_file = remote
                    .iter()
                    .find(|f| f.key == local_file.file_name().unwrap().to_string_lossy());

                if remote_file.is_none() {
                    to_upload.push(local_file);
                }
            }
        }
    }

    Ok((to_upload, to_encrypt))
}
