mod charon;
mod config;
mod crypto;
mod s3;

use std::{fs, path::PathBuf};

use anyhow::Result;
use clap::{Parser, Subcommand};
use config::Config;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct AppCli {
    #[arg(short, long, value_name = "CONFIG_FILE")]
    config: Option<PathBuf>,

    #[command(subcommand)]
    command: Option<AppCliCommands>,
}

#[derive(Subcommand)]
enum AppCliCommands {
    Decrypt {
        #[arg(short, long, value_name = "ENCRYPTED_FILE")]
        input: String,
    },
}

#[tokio::main]
async fn main() -> Result<()> {
    let app = AppCli::parse();
    let c = Box::leak(Box::new(Config::new(
        app.config.unwrap_or("config.toml".into()),
    )?));

    match app.command {
        Some(AppCliCommands::Decrypt { input }) => {
            let src: PathBuf = input.into();
            println!("Decrypting file: {}", src.display());
            let encrypted_data = fs::read(&src)?;
            let decrypted_data = crypto::decrypt(&encrypted_data, &c.encryption_key)?;
            fs::write(src.with_extension(""), decrypted_data)?;
        }
        None => {
            charon::run(c).await?;
        }
    }

    Ok(())
}
