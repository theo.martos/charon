use anyhow::Result;
use s3::{creds::Credentials, Bucket, Region};

use crate::config::Config;

pub async fn get_store(config: &Config) -> Result<Bucket> {
    let bucket_name = config.s3_bucket.clone();
    let region_name = config.aws_region.clone();
    let endpoint = config.s3_endpoint.clone();
    let region = Region::Custom {
        region: region_name,
        endpoint,
    };

    let creds = Credentials::new(
        Some(&config.aws_access_key_id.clone()),
        Some(&config.aws_secret_access_key.clone()),
        None,
        None,
        None,
    )?;

    let bucket = Bucket::new(&bucket_name, region, creds)?;

    Ok(bucket)
}
