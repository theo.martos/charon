use std::{fs, path::Path};

use anyhow::Result;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub backup_dir: String,
    pub aws_access_key_id: String,
    pub aws_secret_access_key: String,
    pub aws_region: String,
    pub s3_bucket: String,
    pub s3_prefix: String,
    pub s3_endpoint: String,
    pub encryption_key: String,
}

impl Config {
    pub fn new<P>(p: P) -> Result<Self>
    where
        P: AsRef<Path>,
    {
        let config_str = fs::read_to_string(p)?;
        let config: Config = toml::from_str(&config_str)?;

        Ok(config)
    }
}
